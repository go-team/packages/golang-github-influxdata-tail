Source: golang-github-influxdata-tail
Section: devel
Priority: optional
Maintainer: Debian Go Packaging Team <pkg-go-maintainers@lists.alioth.debian.org>
Uploaders: Michael Prokop <mika@debian.org>
Rules-Requires-Root: no
Build-Depends:
 debhelper-compat (= 12),
 dh-golang,
 golang-any,
 golang-gopkg-tomb.v1-dev,
 golang-github-fsnotify-fsnotify-dev,
Standards-Version: 4.4.1
Homepage: https://github.com/influxdata/tail
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-influxdata-tail
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-influxdata-tail.git
XS-Go-Import-Path: github.com/influxdata/tail
Testsuite: autopkgtest-pkg-go

Package: golang-github-influxdata-tail-dev
Architecture: all
Depends:
 ${misc:Depends},
 golang-gopkg-tomb.v1-dev,
 golang-github-fsnotify-fsnotify-dev,
Breaks:
 golang-github-hpcloud-tail-dev,
Replaces:
 golang-github-hpcloud-tail-dev,
Provides:
 golang-github-hpcloud-tail-dev,
Multi-Arch: foreign
Description: Go package for reading from continuously updated files (tail -f)
 tail is a Go library striving to emulate the features of the BSD tail program
 (like tail -f). It comes with full support for truncation/move detection
 as it is designed to work with log rotation tools.

Package: gotail
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends}
Built-Using: ${misc:Built-Using}
Description: Go implementation of tail
 gotail is a Go implementation of the tail utility (GNU coreutils).
